[Variables]
ColorText=150,150,150,255           ;light grey
ColorTextLight=255,255,255,255      ;white
ColorPrimary=0,0,0,215              ;black
ColorPrimaryLight=50,50,50,150      ;grey
ColorSecondary=60,220,60,255        ;green
ColorSecondaryLight=60,240,60,255   ;green
